/**
 * General function
 */

let zoomButton = document.querySelector('.zoom');
let zoomContainer = document.querySelector('.container');

zoomButton.addEventListener('click', function(e) {
    e.preventDefault();
    toggleClass(zoomContainer, 'zoomed');
});

/**
 * Sprite animation
 */

let sprites = document.querySelectorAll('.sprite-anim');

if (sprites.length) {

    Spritz('#sprite-knight', {
        picture: {
            srcset: 'https://res.cloudinary.com/dhv9lk6d5/image/upload/v1525715351/gzwuuagknjucna6x4mww.png',
            width: 5870,
            height: 707
        },
        steps: 10
    }).fps(12).play();

    Spritz('#sprite-robot', {
        picture: {
            srcset: 'https://res.cloudinary.com/dhv9lk6d5/image/upload/v1525715349/dsxbildscdmfbpzu22xs.png',
            width: 4536,
            height: 556
        },
        steps: 8
    }).fps(12).play();

    Spritz('#sprite-ninja', {
        picture: {
            srcset: 'https://res.cloudinary.com/dhv9lk6d5/image/upload/v1525715344/ieaqxbxbz5htlz6aomcx.png',
            width: 3630,
            height: 458
        },
        steps: 10
    }).fps(12).play();

}

/**
 * Video animation
 */

let videos = document.getElementsByTagName('video');

if (videos.length) {

    // Firefox doesn't support looping video, so we emulate it this way
    videos.addEventListener('ended', function() {
        video.play();
    }, false);

}
